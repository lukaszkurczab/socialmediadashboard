import React from 'react';
import ReactDOM from 'react-dom';
import Homepage from './pages/Homepage'
import reportWebVitals from './reportWebVitals';
import "./styles/main.css"

ReactDOM.render(
  <React.StrictMode>
    <Homepage />
  </React.StrictMode>,
  document.querySelector('.wrapper')
);

reportWebVitals();
