import React from 'react'
import iconUp from "../images/icon-up.svg"
import iconDown from "../images/icon-down.svg"
import iconFb from "../images/icon-facebook.svg"
import iconIg from "../images/icon-instagram.svg"
import iconTt from "../images/icon-twitter.svg"
import iconYt from "../images/icon-youtube.svg"

class Homepage extends React.Component {
  state = {
    totalFollowers: '23,004',
    darkMode: false,
    platformsData: [
      {
        platform: iconFb,
        name: '@nathanf',
        totalFollowers: '1987',
        changeFollowers: '12',
      },
      {
        platform: iconTt,
        name: '@nathanf',
        totalFollowers: '1044',
        changeFollowers: '99',
      },
      {
        platform: iconIg,
        name: '@realnathanf',
        totalFollowers: '11k',
        changeFollowers: '1099',
      },
      {
        platform: iconYt,
        name: 'Nathan F.',
        totalFollowers: '8239',
        changeFollowers: '-144',
      }
    ],
    overviewsData: [
      {
        platform: iconFb,
        overviewType: 'Page Views',
        overviewNumber: '87',
        overviewChange: '3%'
      },
      {
        platform: iconFb,
        overviewType: 'Likes',
        overviewNumber: '52',
        overviewChange: '-2%'
      },
      {
        platform: iconIg,
        overviewType: 'Likes',
        overviewNumber: '5462',
        overviewChange: '2257%'
      },
      {
        platform: iconIg,
        overviewType: 'Profile Views',
        overviewNumber: '52k',
        overviewChange: '1375%'
      },
      {
        platform: iconTt,
        overviewType: 'Retweets',
        overviewNumber: '117',
        overviewChange: '303%'
      },
      {
        platform: iconTt,
        overviewType: 'Likes',
        overviewNumber: '507',
        overviewChange: '553%'
      },
      {
        platform: iconYt,
        overviewType: 'Likes',
        overviewNumber: '107',
        overviewChange: '-19%'
      },
      {
        platform: iconYt,
        overviewType: 'Total Views',
        overviewNumber: '1407',
        overviewChange: '-12%'
      },
    ]
  }

  followersBox = this.state.platformsData.map((platformData, id) => (
    <div className={`followersBoxes__followersBox ${platformData.platform}`} key={id}>
      <h4 className="followersBoxes__name">
        <img src={`../images/icon-${platformData.platform}.svg`} className="followersBoxes__platformImg" alt=""/>{platformData.name}</h4>
      <h3 className="followersBoxes__totalNumber">{platformData.totalFollowers}</h3>
      <h5 className="followersBoxes__text">FOLLOWERS</h5>
      <h6 className={`followersBoxes__todayNumber ${platformData.changeFollowers > 0 ? "increase" : "decrease"}`}>
        {platformData.changeFollowers > 0 ? <img src={iconUp} className="changeIcon" alt="" /> : <img src={iconDown} className="changeIcon" alt="" />}
        {`${platformData.changeFollowers > 0 ? platformData.changeFollowers : platformData.changeFollowers.substr(1)} Today`}</h6>
    </div>
  ));

  overview = this.state.overviewsData.map((overviewData, id) => (
    <div className="overview__box" key={id}>
      <h4 className="overwiew__text">
        {overviewData.overviewType}
      </h4>
      <img src={overviewData.platform} className="overview__img" alt=""/>
      <h3 className="overview__number">{overviewData.overviewNumber}</h3>
      <h5 className={`overview__change ${overviewData.overviewChange.slice(0, -1) > 0 ? "increase" : "decrease"}`}>
        {overviewData.overviewChange.slice(0, -1) > 0 ? <img src={iconUp} className="changeIcon" alt=""/> : <img src={iconDown} className="changeIcon" alt=""/>}
        {overviewData.overviewChange}</h5>
    </div>
  ))

  changeMode = () => {
    this.setState({
      darkMode: !this.state.darkMode
    })
  }

  render() {
    return (
      <>
        <div className={`${this.state.darkMode ? "darkMode" : ""}`}>
          <header className="header">
            <h1 className="header__title">Social Media Dashboard</h1>
            <h2 className="header__text">Total Followers: {this.state.totalFollowers}</h2>
            <div className="header__darkMode">
              <h2 className="header__text">Dark Mode</h2>
              <div className="header__darkModeBtn" onClick={this.changeMode}>
                <span className="header__darkModeSwitch"></span>
              </div>
            </div>
          </header>
          <main className="main">
            <div className="followersBoxes">
              {this.followersBox}
            </div>
            <div className="overview">
              <h2 className="overview__title">Overview - Today</h2>
              {this.overview}
            </div>
          </main>
          <div className="attribution">
            Challenge by <a href="https://www.frontendmentor.io?ref=challenge">Frontend Mentor</a>. Coded by Łukasz Kurczab.
            </div>
        </div>
      </>
    )
  }
}

export default Homepage